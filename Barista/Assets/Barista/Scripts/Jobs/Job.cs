namespace Barista.Jobs
{
    using Barista.Customers;
    using Barista.Deliveries;
    using Barista.Orders;

    public class Job
    {
        public Customer Customer;

        public Delivery Delivery;

        public Order Order;

        public override string ToString()
        {
            return string.Format("Customer: {0}, Delivery: {1}, Order: {2}", this.Customer, this.Delivery, this.Order);
        }
    }
}