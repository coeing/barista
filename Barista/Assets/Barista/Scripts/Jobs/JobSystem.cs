﻿namespace Barista.Jobs
{
    using System.Collections.Generic;
    using Barista.Customers;
    using Barista.Deliveries;
    using Barista.Orders;
    using UnityEngine;
    using VRTK;

    public class JobSystem : MonoBehaviour
    {
        private Job activeJob;

        public CustomerQueue CustomerQueue;

        public VRTK_SnapDropZone[] finishDropZones;

        private bool jobFinished;

        public TipJar tipJar;

        private float evaluatePerformance()
        {
            var remainingParts =
                new List<CoffeeCombination>(this.activeJob.Order.orderParts);
            List<CoffeeCombination> doneParts =
                new List<CoffeeCombination>();

            float totalTip = 0;
            foreach (var deliveryHolder in this.activeJob.Order.Deliveries)
            {
                // compare type and strength
                var delivery = deliveryHolder.Delivery;

                // first find perfect matches
                foreach (var combi in remainingParts)
                {
                    if (combi.intensity == delivery.coffeeIntensity
                        && combi.type == delivery.coffeeType
                        && !doneParts.Contains(combi))
                    {
                        float currentTip = 10;
                        doneParts.Add(combi);

                        // malus after 30 seconds
                        var dt = Time.timeSinceLevelLoad - this.activeJob.Order.orderTime;
                        //if (dt > 30)
                        //{
                        //    // lineary decrease over the next 30 seconds
                        //    var factor = 1 - (dt - 30) / 30;
                        //    currentTip *= factor;
                        //}

                        totalTip += currentTip;
                    }
                }
            }
            return totalTip;
        }

        private void FinishActiveJob()
        {
            if (this.activeJob == null)
            {
                return;
            }

            // Unregister for events.
            this.activeJob.Customer.DeliveryReceived -= this.OnCustomerReceivedDelivery;

            // Fulfill order.
            this.activeJob.Order.State = OrderState.Fulfilled;

            // Collect deliveries from drop zones.
            this.activeJob.Order.Deliveries = new List<DeliveryHolder>();
            foreach (var z in this.finishDropZones)
            {
                var deliveredObject = z.GetCurrentSnappedObject();
                if (deliveredObject == null)
                {
                    continue;
                }

                var deliveryHolder = deliveredObject.GetComponent<DeliveryHolder>();
                if (deliveryHolder == null)
                {
                    continue;
                }

                this.activeJob.Order.Deliveries.Add(deliveryHolder);
            }

            // Evaluate delivery against order.
            // TODO give money + tip based on performance
            var performanceMoney = this.evaluatePerformance();
            if (this.tipJar != null)
            {
                this.tipJar.addTip(performanceMoney);
            }
            else
            {
                print("tip jar not linked!");
            }

            // TODO remove cups

            Debug.LogFormat("Job finished: {0}, Performance: {1} ", this.activeJob.Order, performanceMoney);

            // TODO inform customer that his order is complete?
            // also inform him about performance so he can comment on it
            this.activeJob.Customer.orderComplete(performanceMoney / 10.0f / activeJob.Order.orderParts.Count);

            this.activeJob = null;
            this.jobFinished = false;
        }

        private void ObjectSnappedToDropZone(object o, SnapDropZoneEventArgs e)
        {
            var deliveryCount = 0;
            foreach (var z in this.finishDropZones)
            {
                if (z.GetCurrentSnappedObject() != null)
                {
                    deliveryCount++;
                }
            }
            this.jobFinished = deliveryCount == this.activeJob.Order.orderParts.Count;

            this.OnCustomerReceivedDelivery(e.snappedObject.GetComponent<DeliveryHolder>().Delivery);
        }

        private void OnCustomerReceivedDelivery(Delivery delivery)
        {
            if (this.activeJob == null)
            {
                Debug.LogWarning("No active job running");
                return;
            }

            this.activeJob.Delivery = delivery;

            if (this.jobFinished)
            {
                this.FinishActiveJob();
            }
        }

        // Use this for initialization
        private void Start()
        {
            foreach (var z in this.finishDropZones)
            {
                z.ObjectSnappedToDropZone += this.ObjectSnappedToDropZone;
            }
        }

        private Job StartNewJob(Customer customer, Order customerOrder)
        {
            // Register for delivieries.
            customer.DeliveryReceived += this.OnCustomerReceivedDelivery;

            customer.showOrder(customerOrder);

            Debug.Log("Start job for order: " + customerOrder);

            // Start new job.
            customerOrder.State = OrderState.Executing;
            return new Job {Customer = customer, Order = customerOrder};
        }

        private Job TryStartNewJob()
        {
            // Check if customer in queue.
            var customer = this.CustomerQueue.GetFirstCustomer();
            if (customer == null)
            {
                return null;
            }

            // Check if customer is observed.
            var customerObserved = customer.GetComponent<ObservedObject>();
            if (customerObserved == null || !customerObserved.IsObserved)
            {
                return null;
            }

            var customerOrder = customer.GetComponent<Order>();
            if (customerOrder == null)
            {
                Debug.LogWarning("Customer has no order", customer);
                return null;
            }

            // TODO: Check distance between customer and player.

            return this.StartNewJob(customer, customerOrder);
        }

        private void Update()
        {
            if (this.activeJob == null)
            {
                // Check if new job should start.
                var newJob = this.TryStartNewJob();
                if (newJob == null)
                {
                    return;
                }

                Debug.Log("New job started: " + newJob);

                this.activeJob = newJob;
            }
        }
    }
}