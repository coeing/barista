﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class PortaCollider : MonoBehaviour {
    public bool isBottom = true;
    public GameObject CoffeePile;
    int bottomCount = 0;
    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Hit Something! " + col.gameObject.name);
        if (col.gameObject.name == "CoffeePile" && isBottom&& bottomCount==0)
        {
            //Debug.Log("Gotcha Bottom!");
            PortaFilterCoffeePile script = CoffeePile.GetComponent<PortaFilterCoffeePile>();
            script.youShallPass = true;
            bottomCount++;
        }
        if (col.gameObject.name == "CoffeePile" && !isBottom)
        {
            Debug.Log("Gotcha Top!");
            PortaFilterCoffeePile script = CoffeePile.GetComponent<PortaFilterCoffeePile>();
            script.youShallPass = true;
            script.coneStep = true;
            bottomCount = 0;
        }

    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
