﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PortaTest : MonoBehaviour {
    public GameObject portaFilter;
    private PortaFilterCoffeePile script;
    public VRTK_SnapDropZone finishDropZones;
    
    
    // Use this for initialization
    //void OnTriggerEnter(Collider col)
    //{
    //    Debug.Log("Found COllision :: " + col.gameObject.name);
    //    if (col.gameObject.name == "CoffeePile" || col.gameObject.name == "BottomCollider" || col.gameObject.name == "Handle")
    //    {
    //        script = portaFilter.GetComponent<PortaFilterCoffeePile>();
    //        script.init();
    //        StartCoroutine(script.StartParticles());
    //        StartCoroutine(script.StartPortaFilter());
    //    }
        

        
    //}
    void Start () {
        Debug.Log("Script attached to " + this.gameObject.name);
        script = portaFilter.GetComponent<PortaFilterCoffeePile>();
        script.init();
        finishDropZones.ObjectSnappedToDropZone += ObjectSnappedToDropZone;


    }
    private void ObjectSnappedToDropZone(object o, SnapDropZoneEventArgs e)
    {
        if (finishDropZones.GetCurrentSnappedObject() != null)
        {
            Debug.Log("Dropped Porta Filter");
            script.init();
            StartCoroutine(script.StartParticles());
            StartCoroutine(script.StartPortaFilter());
        }
    }
        // Update is called once per frame
        void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            script.init();
            StartCoroutine(script.StartParticles());
            StartCoroutine(script.StartPortaFilter());
        }
	}
}
