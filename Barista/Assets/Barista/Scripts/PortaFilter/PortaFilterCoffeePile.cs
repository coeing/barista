﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortaFilterCoffeePile : MonoBehaviour {
    public GameObject portaFilter;
    public GameObject portaFilterTop;
    public GameObject portaFilterCone;
    public GameObject portaFilterBottom;
    public float initialSpeed= 0.00104f;
    public float riseYSpeed = 0.001f;
    public ParticleSystem particleSystem;
    public float rate;
    private Vector3 portaFilterBaseScale;
    private bool initialStep;
    private bool cylinderStep;
    public bool coneStep;
    public bool youShallPass = true;
    private float startTime;
    private float volume;
    private float step1CutoffVolume;
    private bool machineState;
    private Animator animator;
    public void init()
    {
        StartParticles();
        machineState = false;
        this.gameObject.transform.position = portaFilter.transform.position;
        portaFilterBaseScale = this.gameObject.transform.localScale;
        this.gameObject.transform.localScale = new Vector3(0.0f, portaFilterBaseScale.y, 0.0f);
        initialStep = true;
        startTime = Time.time;
        step1CutoffVolume = Mathf.PI * (portaFilterBaseScale.z / 2) * (portaFilterBaseScale.z / 2) * portaFilterBaseScale.y;
        animator = portaFilterCone.GetComponent<Animator>();
        Debug.Log("CutoffVolume : " + step1CutoffVolume);
        portaFilterCone.GetComponent<MeshRenderer>().enabled = false;
        coneStep = false;
    }

    private float deltaRadiusStep1()
    {
        return initialSpeed;//0.00104f;//Mathf.Sqrt(rate / (portaFilterBaseScale.y*Mathf.PI));
    }
    private float getTime()
    {
        return Time.time - startTime;
    }
    /// <summary>
    /// Use this function to start PortaFilter Animation
    /// </summary>
    public IEnumerator StartPortaFilter()
    {
        while (!machineState)
        {
            yield return null;
        }
        volume = getTime() * rate;
        float deltaRad = deltaRadiusStep1();
        this.gameObject.GetComponent<MeshRenderer>().enabled = true;
        // Debug.Log("Input Volume : " + volume + " Delta : " + deltaRad);
        youShallPass = false;
            while (!youShallPass)
            {
                // Debug.Log("Volume : " + volume);
                this.gameObject.transform.localScale += new Vector3(deltaRad, 0.0f, deltaRad);
                yield return null;
            }
            //if (this.gameObject.transform.localScale.x > portaFilterBaseScale.x)
            //{
            //    this.gameObject.transform.localScale = portaFilterBaseScale;
            cylinderStep = true; initialStep = false;
            //}
            yield return false;

            youShallPass = false;
            while (!coneStep)
            {
                this.gameObject.transform.position += new Vector3(0.0f, riseYSpeed, 0.0f);
                yield return null;
            }

        portaFilterCone.GetComponent<MeshRenderer>().enabled = true;
        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        portaFilterCone.transform.position = new Vector3(portaFilterTop.transform.position.x, portaFilterTop.transform.position.y + 0.015f, portaFilterTop.transform.position.z);
        int triggerHash = Animator.StringToHash("conetrigger");
        animator.SetTrigger(triggerHash);

        coneStep = false;

        StartCoroutine(StopParticles());


    }
    /// <summary>
    /// When the controller is removed, call this function to signal a stop call to grinder.
    /// </summary>
    /// <returns></returns>
    public IEnumerator StopParticles()
    {
        machineState = false;
        yield return new WaitForSeconds(3);
        particleSystem.Stop();
    }
    public IEnumerator StartParticles()
    {
        
        yield return new WaitForSeconds(2);
        machineState = true;
        particleSystem.Play();
    }
    
    // Use this for initialization
    void Start()
    {
        //init();
        StartCoroutine(StopParticles());
    }
    // Update is called once per frame
    void Update () {
       // StartPortaFilter();
	}
}
