﻿namespace Barista.Players
{
    using UnityEngine;

    public class Player : MonoBehaviour
    {
        public Vector3 Position
        {
            get
            {
                if (Camera.main != null)
                {
                    return Camera.main.transform.position;
                }
                return Vector3.zero;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                if (Camera.main != null)
                {
                    return Camera.main.transform.rotation;
                }
                return Quaternion.identity;
            }
        }
    }
}