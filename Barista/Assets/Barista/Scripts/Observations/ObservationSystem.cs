﻿namespace Barista.Observations
{
    using System.Collections.Generic;
    using System.Runtime.Remoting.Messaging;
    using Barista.Customers;
    using Barista.Players;
    using UnityEngine;

    public class ObservationSystem : MonoBehaviour
    {
        public List<ObservedObject> ObservedObjects;

        public Player Player;

        private bool IsObjectObserved(Player player, ObservedObject observedObject)
        {
            // Get look direction of player.
            var playerLookDirection = player.Rotation.eulerAngles;
            var objectDirection = Quaternion.LookRotation(observedObject.transform.position - this.Player.Position)
                .eulerAngles;

            // Just consider XZ plane.
            var angleToObject = Mathf.Abs(Mathf.DeltaAngle(objectDirection.y, playerLookDirection.y));

            return angleToObject < observedObject.MaxObserveAngle;
        }

        private void OnDrawGizmosSelected()
        {
            if (this.Player == null || this.ObservedObjects == null)
            {
                return;
            }

            foreach (var observedObject in this.ObservedObjects)
            {
                Gizmos.color = observedObject.IsObserved ? Color.green : Color.red;
                Gizmos.DrawLine(this.Player.Position, observedObject.transform.position);
            }
        }

        private void Update()
        {
            // Update observation flags.
            foreach (var observedObject in this.ObservedObjects)
            {
                observedObject.IsObserved = this.IsObjectObserved(this.Player, observedObject);
            }
        }
    }
}