﻿namespace Barista.Customers
{
    using Barista.Observations;
    using UnityEngine;

    /// <summary>
    ///     Placed on an object that can be observed.
    /// </summary>
    public class ObservedObject : MonoBehaviour
    {
        private bool isObserved;

        [Range(0, 90)]
        public float MaxObserveAngle = 10.0f;

        /// <summary>
        ///     Indicates if object is currently observed.
        /// </summary>
        public bool IsObserved
        {
            get
            {
                return this.isObserved;
            }
            set
            {
                if (value == this.isObserved)
                {
                    return;
                }

                this.isObserved = value;
            }
        }

        private void OnDisable()
        {
            // Unregister from observation system.
            var observationSystem = FindObjectOfType<ObservationSystem>();
            if (observationSystem != null)
            {
                observationSystem.ObservedObjects.Remove(this);
            }
        }

        private void OnEnable()
        {
            // Register at observation system.
            var observationSystem = FindObjectOfType<ObservationSystem>();
            if (observationSystem != null)
            {
                observationSystem.ObservedObjects.Add(this);
            }
        }
    }
}