﻿using System;
using System.Collections;
using System.Collections.Generic;
using Barista.Orders;
using UnityEngine;

public class OrderVisualization : MonoBehaviour {

    public Order order;
    public Sprite[] orderSprites;
    public Sprite[] feedbackSprites;
    public Sprite[] bgSprites;
    public SpriteRenderer[] spriteRenderers;
    public SpriteRenderer bgRenderer;

    // Use this for initialization
    void Start () {
        enabled = false;
        updateOrderVisuals();
	}

    private void updateOrderVisuals()
    {
        if (this.order == null)
        {
            print("ERROR: No order!");
            return;
        }

        int spriteID = 0;
        int orderCount = 0;
        foreach (CoffeeCombination p in order.orderParts)
        {
            var scale = spriteRenderers[spriteID].transform.localScale;
            switch (p.intensity)
            {
                case CoffeeIntensity.Strong:
                    
                    spriteRenderers[spriteID].sprite = p.type == CoffeeType.Coffee ? orderSprites[0] : orderSprites[2];
                    break;

                case CoffeeIntensity.Weak:
                    spriteRenderers[spriteID].sprite = p.type == CoffeeType.Coffee ? orderSprites[1] : orderSprites[3];
                    break;

                default:
                    Debug.LogError("wrong coffee type");
                    break;
            }
            spriteRenderers[spriteID].transform.localScale = scale;
            ++spriteID;
            ++orderCount;
        }

        // adjust bg size
        bgRenderer.sprite = bgSprites[orderCount - 1];
        enabled = true;

    }

    internal void setFeedback(float performance)
    {
        bgRenderer.sprite = bgSprites[0];
        // performance money ranges from 0 to 1
        if (performance > 0.8)
            spriteRenderers[0].sprite = feedbackSprites[0];
         else if (performance > 0.2)
            spriteRenderers[0].sprite = feedbackSprites[1];
        else
            spriteRenderers[0].sprite = feedbackSprites[2];
        spriteRenderers[1].enabled = false;
        spriteRenderers[2].enabled = false;
    }

    public void setOrder (Order order)
    {
        this.order = order;
        updateOrderVisuals();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
