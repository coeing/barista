﻿namespace Barista.Orders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Barista.Deliveries;
    using UnityEngine;
    using Random = System.Random;

    public class Order : MonoBehaviour
    {
        // Espresso, Caffee; each can be strong or weak
        public List<CoffeeCombination> orderParts = new List<CoffeeCombination>();
        public float orderTime;

        public List<DeliveryHolder> Deliveries = new List<DeliveryHolder>();

        public OrderState State { get; set; }

        private static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T) v.GetValue(new Random().Next(v.Length));
        }

        // Use this for initialization
        private void Start()
        {
            // some random order
            var maxOrderParts = 3; // this is fixed by the order visual
            for (var i = 0; i < UnityEngine.Random.Range(1, maxOrderParts); ++i)
            {
                var randomIntensity = RandomEnumValue<CoffeeIntensity>();
                while (randomIntensity == CoffeeIntensity.Water)
                {
                    randomIntensity = RandomEnumValue<CoffeeIntensity>();
                }

                var randomType = RandomEnumValue<CoffeeType>();
                this.orderParts.Add(new CoffeeCombination(randomType, randomIntensity));
                orderTime = Time.timeSinceLevelLoad;
            }
        }

        // Update is called once per frame
        private void Update()
        {
        }

        public override string ToString()
        {
            return string.Format("State: {0}, OrderParts: {1}, Delivieries: {2}", this.State,
                this.orderParts != null
                    ? string.Join("; ", this.orderParts.Select(orderPart => orderPart.ToString()).ToArray())
                    : "null",
                this.Deliveries != null
                    ? string.Join("; ", this.Deliveries.Select(delivery => delivery.Delivery.ToString()).ToArray())
                    : "null");
        }
    }

    public enum OrderState
    {
        Waiting,

        Executing,

        Fulfilled
    }

    public struct CoffeeCombination
    {
        public CoffeeType type;

        public CoffeeIntensity intensity;

        public CoffeeCombination(CoffeeType type, CoffeeIntensity intensity)
        {
            this.type = type;
            this.intensity = intensity;
        }

        public override string ToString()
        {
            return string.Format("Type: {0}, Intensity: {1}", this.type, this.intensity);
        }
    }

    public enum CoffeeIntensity
    {
        Water,

        Weak,

        Strong
    }

    public enum CoffeeType
    {
        Espresso,

        Coffee
    }
}