﻿namespace Barista.Customers
{
    using System;
    using Barista.Deliveries;
    using Barista.Orders;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class Customer : MonoBehaviour
    {
        private static int runningID = 0;
        public Sprite[] customerMaterials;

        public GameObject orderVisual;

        public SpriteRenderer quadMesh;

        public event Action<Delivery> DeliveryReceived;

        public event Action<Customer> RemovedFromQueue;

        public virtual void OnDeliveryReceived(Delivery delivery)
        {
            var handler = this.DeliveryReceived;
            if (handler != null)
            {
                handler(delivery);
            }
        }

        public virtual void OnRemovedFromQueue()
        {
            var handler = this.RemovedFromQueue;
            if (handler != null)
            {
                handler(this);
            }
        }

        public void showOrder(Order order)
        {
            this.orderVisual.GetComponent<OrderVisualization>().setOrder(order);
        }

        private void Start()
        {
            print("start!!!");
            //this.quadMesh.sprite = this.customerMaterials[Random.Range(0, this.customerMaterials.Length)];
            this.quadMesh.sprite = this.customerMaterials[runningID];
            runningID++;
            runningID = runningID % this.customerMaterials.Length;
        }

        internal void orderComplete(float performance)
        {
           this.orderVisual.GetComponent<OrderVisualization>().setFeedback(performance);
        }
    }
}