﻿namespace Barista.Customers
{
    using UnityEngine;

    public class CustomerSpot : MonoBehaviour
    {
        /// <summary>
        ///     Observed state of spot.
        /// </summary>
        public ObservedObject Observed;

        /// <summary>
        ///     Provides position of the spot.
        /// </summary>
        public Transform Transform;

        /// <summary>
        ///     Customer on this spot.
        /// </summary>
        public Customer Customer { get; private set; }

        public void AssignCustomer(Customer customer)
        {
            this.Customer = customer;

            if (this.Customer != null)
            {
                this.Customer.transform.position = this.Transform.position;
            }
        }

        public void RemoveCustomer()
        {
            this.Customer = null;
        }

        private void Reset()
        {
            this.Transform = this.transform;
        }
    }
}