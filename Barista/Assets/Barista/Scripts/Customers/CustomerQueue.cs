﻿namespace Barista.Customers
{
    using System.Linq;
    using Barista.Orders;
    using UnityEngine;

    public class CustomerQueue : MonoBehaviour
    {
        /// <summary>
        ///     Delivery spot for customer.
        /// </summary>
        public CustomerSpot DeliverySpot;

        /// <summary>
        ///     Observed state of customer queue.
        /// </summary>
        public ObservedObject Observed;

        /// <summary>
        ///     Spots that belong to queue.
        /// </summary>
        public CustomerSpot[] Spots;

        public Customer GetFirstCustomer()
        {
            return this.Spots.Select(customerSpot => customerSpot.Customer).FirstOrDefault();
        }

        /// <summary>
        ///     Indicates if any spot in the queue is free.
        /// </summary>
        /// <returns>Returns a free spot if there is any.</returns>
        public CustomerSpot GetFreeSpot()
        {
            return this.Spots.FirstOrDefault(customerSpot => customerSpot.Customer == null);
        }

        private void TryMoveCustomerToDeliverySpot()
        {
            if (this.DeliverySpot == null)
            {
                return;
            }

            // Check if delivery spot is free.
            if (this.DeliverySpot.Customer != null)
            {
                return;
            }

            var firstSpot = this.Spots.FirstOrDefault();
            if (firstSpot == null)
            {
                return;
            }

            var customer = firstSpot.Customer;
            if (customer == null)
            {
                return;
            }

            // Check if order of customer is served or fulfilled.
            var customerOrder = customer.GetComponent<Order>();
            if (customerOrder != null && customerOrder.State == OrderState.Waiting)
            {
                return;
            }

            // Remove from old spot.
            firstSpot.RemoveCustomer();

            // Assign to new spot.
            this.DeliverySpot.AssignCustomer(customer);
        }

        private void TryRemoveCustomerFromDeliverySpot()
        {
            if (this.DeliverySpot == null)
            {
                return;
            }

            // Check if delivery spot is observed.
            if (this.DeliverySpot.Observed != null && this.DeliverySpot.Observed.IsObserved)
            {
                return;
            }

            var customer = this.DeliverySpot.Customer;
            if (customer == null)
            {
                return;
            }

            // Check if order of customer is fulfilled.
            var customerOrder = customer.GetComponent<Order>();
            if (customerOrder != null && customerOrder.State != OrderState.Fulfilled)
            {
                return;
            }

            // Remove customer.
            this.DeliverySpot.RemoveCustomer();

            if (customerOrder != null)
            {
                // Remove deliveries of order.
                foreach (var delivery in customerOrder.Deliveries)
                {
                    Destroy(delivery.gameObject);
                }
            }

            // Invoke event.
            customer.OnRemovedFromQueue();
        }

        private void Update()
        {
            // Update queue when player is not looking.
            if (this.Observed == null || !this.Observed.IsObserved)
            {
                this.TryRemoveCustomerFromDeliverySpot();
                this.TryMoveCustomerToDeliverySpot();
            }
        }
    }
}