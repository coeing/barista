﻿namespace Barista.Customers
{
    using Barista.Orders;
    using Barista.Players;
    using UnityEngine;

    /// <summary>
    ///     Spawns customers when the player is not looking at the door.
    /// </summary>
    public class CustomerSpawner : MonoBehaviour
    {
        /// <summary>
        ///     Cooldown between two customer spawns.
        /// </summary>
        [Range(0, 20)]
        public float CooldownBetweenSpawns = 10.0f;

        public GameObject CustomerPrefab;

        public ObservedObject Door;

        public Player Player;

        public CustomerQueue Queue;

        /// <summary>
        ///     Remaining cooldown before a new customer can be spawned.
        /// </summary>
        private float remainingCooldown;

        public void Update()
        {
            this.remainingCooldown -= Time.deltaTime;

            // Check if cool down is running.
            if (this.remainingCooldown > 0)
            {
                return;
            }

            //  Check for free slot in queue.
            var freeSpot = this.Queue.GetFreeSpot();
            if (freeSpot == null)
            {
                return;
            }

            // Check if player is looking at door.
            if (this.Door != null && this.Door.IsObserved)
            {
                return;
            }

            // Spawn new customer on spot.
            var customer = this.CreateCustomer();
            freeSpot.AssignCustomer(customer);

            // Set cooldown.
            this.remainingCooldown = this.CooldownBetweenSpawns;

            // TODO: Play bell ring sound.
        }

        private Customer CreateCustomer()
        {
            var customerGameObject = Instantiate(this.CustomerPrefab);

            var customer = customerGameObject.GetComponent<Customer>() ?? customerGameObject.AddComponent<Customer>();

            // Register for event to destroy customer.
            customer.RemovedFromQueue += this.OnCustomerRemovedFromQueue;

            // Add order.
            customerGameObject.AddComponent<Order>();

            return customerGameObject.GetComponent<Customer>();
        }

        private void OnCustomerRemovedFromQueue(Customer customer)
        {
            this.RemoveCustomer(customer);
        }

        private void RemoveCustomer(Customer customer)
        {
            // Unregister from events.
            customer.RemovedFromQueue -= this.OnCustomerRemovedFromQueue;

            // Destroy game object.
            Destroy(customer.gameObject);
        }
    }
}