﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipJar : MonoBehaviour {
    public MeshRenderer topPart;
    public GameObject fillPart;

    // exposed for debugging only
    public float fillLevel = 0;
    public float maxMoney = 100;
	public float moneyThres1 = 1;
	public float moneyThres2 = 5;

    private Vector3 topMaxPosition = new Vector3(0, 0.153f, 0);
    private Vector3 buttomMaxPosition = new Vector3(0, 0, 0);

    private Vector3 maxScale = new Vector3(1, 0.07871658f, 1);
    private Vector3 minScale = new Vector3(1, 0, 1);

	private AudioSource audioSource;
	private AudioClip money1, money2, money3;

    // Use this for initialization
    void Start () 
	{
		audioSource = GetComponent<AudioSource> ();
    }

    public void addTip(float amount)
    {
        fillLevel += amount / maxMoney;

		if (amount < moneyThres1) {
			audioSource.PlayOneShot (money1);
		} else if (amount < moneyThres2) {
			audioSource.PlayOneShot (money2);
		} else {
			audioSource.PlayOneShot (money3);
		}
    }
	
	// Update is called once per frame
	void Update () {
        topPart.transform.localPosition = Vector3.Lerp(buttomMaxPosition, topMaxPosition, fillLevel);
        fillPart.transform.localScale = Vector3.Lerp(minScale, maxScale, fillLevel);
    }
}
