﻿namespace Barista.Controls
{
    using UnityEngine;
    public class PortaFilter : MonoBehaviour
    {
        public PortaFilterType FilterType;
    }

    public enum PortaFilterType
    {
        None,
        Small,
        Big
    }
}