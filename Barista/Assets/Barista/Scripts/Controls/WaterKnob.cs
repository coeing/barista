﻿namespace Barista.Controls
{
    using System;
    using UnityEngine;
    using UnityEngine.Events;
    using VRTK;

    public class WaterKnob : MonoBehaviour
    {
        public VRTK_Knob Knob;

        public VRTK_InteractableObject KnobInteractable;

        public WaterAmountChosenEvent WaterAmountChosen;

        public void ResetKnobPosition()
        {
            this.SetKnobPosition(WaterAmount.None);
        }

        private void OnDisable()
        {
            if (this.KnobInteractable != null)
            {
                this.KnobInteractable.InteractableObjectUngrabbed -= this.OnKnobUngrabbed;
            }
        }

        private void OnEnable()
        {
            if (this.KnobInteractable != null)
            {
                this.KnobInteractable.InteractableObjectUngrabbed += this.OnKnobUngrabbed;
            }
        }

        private void OnKnobUngrabbed(object sender, InteractableObjectEventArgs e)
        {
            Debug.Log("Ungrabbed");

            if (this.Knob == null)
            {
                return;
            }
            var knobValue = this.Knob.GetValue();
            var waterAmount = WaterAmount.None;
            if (Math.Abs(knobValue) < 150)
            {
                if (knobValue > 0)
                {
                    waterAmount = WaterAmount.Single;
                }
                else if (knobValue < 0)
                {
                    waterAmount = WaterAmount.Double;
                }
            }

            // Snap to water amount.
            this.SetKnobPosition(waterAmount);

            this.WaterAmountChosen.Invoke(waterAmount);
        }

        private void SetKnobPosition(WaterAmount waterAmount)
        {
            if (this.KnobInteractable == null)
            {
                return;
            }

            var knobDirection = 0;
            switch (waterAmount)
            {
                case WaterAmount.None:
                    break;
                case WaterAmount.Single:
                    knobDirection = -35;
                    break;
                case WaterAmount.Double:
                    knobDirection = 35;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("waterAmount", waterAmount, null);
            }

            var knobRotation = this.KnobInteractable.transform.rotation.eulerAngles;
            this.KnobInteractable.transform.rotation =
                Quaternion.Euler(knobRotation.x, knobRotation.y, knobDirection);
        }

        [Serializable]
        public class WaterAmountChosenEvent : UnityEvent<WaterAmount>
        {
        }
    }

    public enum WaterAmount
    {
        None,

        Single,

        Double
    }
}