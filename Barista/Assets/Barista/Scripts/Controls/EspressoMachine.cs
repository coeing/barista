﻿namespace Barista.Controls
{
    using System;
    using Barista.Deliveries;
    using Barista.Orders;
    using UnityEngine;
    using VRTK;

    public class EspressoMachine : MonoBehaviour
    {
        public VRTK_SnapDropZone CupDropZone;

        public VRTK_SnapDropZone FilterDropZone;

        public WaterKnob WaterKnob;

        public void StartBrewing(WaterAmount waterAmount)
        {
            Debug.Log("Start brewing with " + waterAmount);

            if (waterAmount == WaterAmount.None)
            {
                return;
            }

            // Get cup.
            var cup = this.GetCup();
            var filter = this.GetFilter();
            if (cup != null && cup.FillLevel != null)
            {
                float additionalFillLevel;
                switch (cup.Delivery.coffeeType)
                {
                    case CoffeeType.Espresso:
                        additionalFillLevel = 1.0f;
                        break;
                    case CoffeeType.Coffee:
                        additionalFillLevel = waterAmount == WaterAmount.Single ? 1.0f : 0.5f;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                // Consider filter.
                PortaFilterType filterType;
                if (filter != null)
                {
                    filterType = filter.FilterType;
                }
                else
                {
                    filterType = PortaFilterType.None;
                    Debug.LogWarning("No filter in machine");
                }

                CoffeeIntensity coffeeIntensity;
                switch (filterType)
                {
                    case PortaFilterType.None:
                        coffeeIntensity = CoffeeIntensity.Water;
                        break;
                    case PortaFilterType.Small:
                        coffeeIntensity = CoffeeIntensity.Weak;
                        break;
                    case PortaFilterType.Big:
                        coffeeIntensity = CoffeeIntensity.Strong;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Debug.LogFormat("Add fill level {0} for coffee type {1} with filter {2}", additionalFillLevel,
                    cup.Delivery.coffeeType, filterType);

                // Fill cup.
                cup.FillLevel.fillLevel += additionalFillLevel;
                cup.FillLevel.fillLevel = Mathf.Clamp01(cup.FillLevel.fillLevel);

                // Adjust coffee intensity.
                cup.Delivery.coffeeIntensity = coffeeIntensity;
            }
            else
            {
                Debug.LogWarning("No cup in machine");
            }

            // Reset water knob.
            if (this.WaterKnob != null)
            {
                this.WaterKnob.ResetKnobPosition();
            }
        }

        private DeliveryHolder GetCup()
        {
            if (this.CupDropZone == null)
            {
                return null;
            }

            var snappedObject = this.CupDropZone.GetCurrentSnappedObject();
            if (snappedObject == null)
            {
                return null;
            }

            return snappedObject.GetComponent<DeliveryHolder>();
        }

        private PortaFilter GetFilter()
        {
            if (this.FilterDropZone == null)
            {
                return null;
            }

            var snappedObject = this.FilterDropZone.GetCurrentSnappedObject();
            if (snappedObject == null)
            {
                return null;
            }

            return snappedObject.GetComponent<PortaFilter>();
        }
    }
}