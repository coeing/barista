﻿namespace Barista.Controls
{
    using System;
    using System.Collections.Generic;
    using Barista.Deliveries;
    using Barista.Orders;
    using UnityEngine;

    public class CupStorage : MonoBehaviour
    {
        public GameObject CoffeeCupPrefab;

        public List<DeliveryHolder> Cups;

        private List<CupSlot> cupSlots;

        public float DistanceToSpawnNewCup = 1.0f;

        public GameObject EspressoCupPrefab;

        private Transform CreateNewCup(CupSlot cupSlot)
        {
            GameObject cupPrefab;
            switch (cupSlot.CupType)
            {
                case CoffeeType.Espresso:
                    cupPrefab = this.EspressoCupPrefab;
                    break;
                case CoffeeType.Coffee:
                    cupPrefab = this.CoffeeCupPrefab;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (cupPrefab == null)
            {
                return null;
            }

            var cupGameObject = Instantiate(cupPrefab, cupSlot.Position, Quaternion.identity);
            return cupGameObject.transform;
        }

        private void Start()
        {
            // Create initial cup slots.
            this.cupSlots = new List<CupSlot>();
            foreach (var cup in this.Cups)
            {
                var cupSlot = new CupSlot
                {
                    Position = cup.transform.position,
                    Content = cup.transform,
                    CupType = cup.Delivery.coffeeType
                };
                this.cupSlots.Add(cupSlot);
            }
        }

        private void Update()
        {
            if (this.cupSlots == null)
            {
                return;
            }

            // Check if a slot needs a new cup.
            foreach (var cupSlot in this.cupSlots)
            {
                if (cupSlot.Content != null)
                {
                    var contentDistanceToSlot = (cupSlot.Content.position - cupSlot.Position).magnitude;
                    if (contentDistanceToSlot > this.DistanceToSpawnNewCup)
                    {
                        cupSlot.Content = null;
                    }
                }

                if (cupSlot.Content == null)
                {
                    cupSlot.Content = this.CreateNewCup(cupSlot);
                }
            }
        }
    }

    public class CupSlot
    {
        public Vector3 Position { get; set; }

        public Transform Content { get; set; }

        public CoffeeType CupType { get; set; }
    }
}