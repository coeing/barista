namespace Barista.Deliveries
{
    using System;
    using Barista.Orders;

    [Serializable]
    public class Delivery
    {
        public CoffeeIntensity coffeeIntensity;

        public CoffeeType coffeeType;

        // times Time.timeSinceLevelLoad
        public float finishedFillTime;

        public float startFillTime;

        public override string ToString()
        {
            return string.Format("CoffeeType: {0}, CoffeeIntensity: {1}, FinishedFillTime: {2}, StartFillTime: {3}",
                this.coffeeType, this.coffeeIntensity, this.finishedFillTime, this.startFillTime);
        }
    }
}