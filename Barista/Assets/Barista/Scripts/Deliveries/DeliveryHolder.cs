﻿namespace Barista.Deliveries
{
    using UnityEngine;
    public class DeliveryHolder : MonoBehaviour
    {
        public Delivery Delivery;

        public FillLevel FillLevel;
    }
}