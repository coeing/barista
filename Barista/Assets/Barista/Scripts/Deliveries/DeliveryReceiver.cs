﻿namespace Barista.Deliveries
{
    using System;
    using UnityEngine;
    using UnityEngine.Events;
    using VRTK;

    public class DeliveryReceiver : MonoBehaviour
    {
        public DeliveryReceivedEvent DeliveryReceived;

        private void OnDeliveryHolderTouched(DeliveryHolder deliveryHolder)
        {
            if (deliveryHolder.Delivery == null)
            {
                Debug.LogWarning("No delivery in DeliveryHolder");
                return;
            }
            this.DeliveryReceived.Invoke(deliveryHolder.Delivery);
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            var interactableObject = otherCollider.GetComponentInParent<VRTK_InteractableObject>();
            if (interactableObject == null)
            {
                return;
            }
            var deliveryHolder = interactableObject.GetComponentInChildren<DeliveryHolder>();
            if (deliveryHolder == null)
            {
                return;
            }
            this.OnDeliveryHolderTouched(deliveryHolder);
        }

        [Serializable]
        public class DeliveryReceivedEvent : UnityEvent<Delivery>
        {
        }
    }
}