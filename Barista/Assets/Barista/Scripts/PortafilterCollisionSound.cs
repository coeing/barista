﻿using System.Collections;
using UnityEngine;

public class PortafilterCollisionSound : MonoBehaviour 
{
	AudioSource audioSource;

	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	void OnCollisionEnter (Collision col) 
	{			
		if (col.relativeVelocity.magnitude > 1)
			audioSource.Play();
	}
}