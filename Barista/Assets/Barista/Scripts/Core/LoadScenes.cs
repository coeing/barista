﻿namespace Barista.Core
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class LoadScenes : MonoBehaviour
    {
        public string[] Scenes;

        private void Start()
        {
            foreach (var scene in this.Scenes)
            {
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);
            }
        }
    }
}