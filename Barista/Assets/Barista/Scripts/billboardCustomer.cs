﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class billboardCustomer : MonoBehaviour {
    Camera cam;

    // Use this for initialization
    void Start () {
        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (this.cam == null)
	    {
	        cam = Camera.main;
            return;
        }

        // only rotate y axis
        Vector3 rot = Quaternion.LookRotation(transform.position - cam.transform.position).eulerAngles;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = Quaternion.Euler(rot);
    }
}
