﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FillLevel : MonoBehaviour {
    public GameObject fillMesh;
    public GameObject cupMesh;
    public GameObject minFillTarget;
    


    // full (1) to empty (0)
    // this will get decrease when the cup is not leveled
    [Range(0, 1)]
    public float fillLevel = 1.0f;

    // current acceleration, we keep it 0,0,0 first, 
    // so only gravity influences the level
    Vector3 acceleration = new Vector3(0,0,0);
    static Vector3 gravity = new Vector3(0, -9.8f, 0);

    // used for calculation surface calculation
    float fillScale;
    // initial s value which also is our r
    float r;

    // position of the cup's floor relative to cup
    //Vector3 minFillPositionLocal;
    float heightScale;
    

	// Use this for initialization
	void Start () {
        // we use this as radius of our cup
        r = fillMesh.transform.localScale.x / 2.0f;
        heightScale = (minFillTarget.transform.localPosition - fillMesh.transform.localPosition).magnitude;

        // we expect the scale to go from 0 to 1 for min to max
        // this is the area of of a half cirle and the area of a fully filled cup
        fillScale = Mathf.PI * r * r;
    }

    // Update is called once per frame
    void Update () {
        //fillLevel = (Mathf.Sin(Time.timeSinceLevelLoad) + 1 ) / 2.0f;

        // compute maximum possible volume for current cup orientation
        // we approximate this by usind all 3 axis seperately and taking the min
        // so we overestimate
        List<float> areas = new List<float>();
        areas.Add(calcArea(90 - transform.rotation.eulerAngles.x));
        areas.Add(calcArea(90 + transform.rotation.eulerAngles.x));
        // we ignore y here, as this is our up axis
        areas.Add(calcArea(90 - transform.rotation.eulerAngles.z));
        areas.Add(calcArea(90 + transform.rotation.eulerAngles.z));
        

        float maxArea = areas.Min() / 2;
        // area based on fill level
        float area = fillScale * fillLevel;
        if (area > maxArea)
        {
            //print("spill");
            area = maxArea;

            float newLevel = area / fillScale;
            spill(fillLevel - newLevel);
            fillLevel = newLevel;
        }


        float s = Mathf.Sqrt(area / Mathf.PI);
        // actually +/- in the formular
        float h = - Mathf.Sqrt(r * r - (area / Mathf.PI) );
        if (float.IsNaN(h))
        {
            h =0;
        }

        // scale the diameter of the circle
        Vector3 scale = fillMesh.transform.localScale;
        scale.x = s * 2;
        scale.y = s * 2;
        fillMesh.transform.localScale = scale;

        // find the "buttom" of the cup
        RaycastHit hit;
        // fall back to ground pos if not hit
        Vector3 groundPos = minFillTarget.transform.position;
        if (Physics.Raycast(transform.position, gravity, out hit, 1))
        {
            groundPos = hit.point;
        }

        // adjust the elevation
        Vector3 pos = groundPos;

        pos.y = transform.position.y + h / r * heightScale * 2;
        fillMesh.transform.position = pos;

        // update orientation to match gravity (and acceleration?! TODO)
        fillMesh.transform.rotation =  Quaternion.Euler(270, 0, 0);
    }

    float calcArea(float omega)
    {
        return 2 * Mathf.PI * r * r * (1 - Mathf.Cos(Mathf.Deg2Rad * omega));
    }

    // amount in percentage of cup volume
    void spill(float amount)
    {
        var ps = GetComponentInParent<ParticleSystem>();
        ps.Play();
        // TODO: add particle effect
        // add current velocity to particles
    }
}
